package com.example.amartineza.loginfragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements  LoginFragment.InterfaceGoToFormFragment{

    LoginFragment loginFragment;
    FormFragment formFragment;
    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginFragment = loginFragment.newInstance();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_fragment, loginFragment, "Login Fragment");
        fragmentTransaction.commit();
    }

    @Override
    public void goToFromFragment(String userName) {
        formFragment = formFragment.newInstance(userName);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.layout_fragment, formFragment, "Form Fragment");
        fragmentTransaction.commit();
    }
}

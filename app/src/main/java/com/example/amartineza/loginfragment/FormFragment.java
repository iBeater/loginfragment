package com.example.amartineza.loginfragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FormFragment extends Fragment {
    List<FormModel> list = new ArrayList<>();
    private FormAdapter adapter;
    Context context;
    private RecyclerView rvForm;

    EditText etUserName;
    EditText etUserAge;
    EditText etUserAddress;
    RadioButton rbUserFemale;
    RadioGroup rgUserSex;
    Button btnSaveForm;

    String userName;
    String userAge;
    String userAddress;
    String userSex;


    public FormFragment() {
        // Required empty public constructor
    }

    public static FormFragment newInstance(String userName) {
        FormFragment fragment = new FormFragment();
        fragment.userName = userName;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_form, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this.getContext();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etUserName = view.findViewById(R.id.et_user_name);
        etUserAge = view.findViewById(R.id.et_user_age);
        etUserAddress = view.findViewById(R.id.et_user_address);
        rgUserSex= view.findViewById(R.id.rg_users_sex);
        btnSaveForm = view.findViewById(R.id.btn_save);
        rvForm = view.findViewById(R.id.form_recycler);

        startAdapter();

        btnSaveForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateForm()) {
                    userName = etUserName.getText().toString();
                    userAge = etUserAge.getText().toString();
                    userAddress = etUserAddress.getText().toString();
                    FormModel model = new FormModel(userName, userAge, userSex, userAddress);
                    list.add(model);
                   adapter.notifyDataSetChanged();
                    etUserName.setText("");
                    etUserAge.setText("");
                    etUserAddress.setText("");
                    rgUserSex.clearCheck();
                }
            }
        });

        rgUserSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_female:
                        userSex = "Femenino";
                        break;
                    case R.id.rb_male:
                        userSex = "Masculino";
                        break;
                }
            }
        });
    }

    public void startAdapter() {
        adapter = new FormAdapter(context, list);
        rvForm.setAdapter(adapter);
        rvForm.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        rvForm.hasFixedSize(); //siempre tienen el mismo tamaño
    }


    public Boolean validateForm() {
        Boolean itsValid = true;
        if (etUserName.getText().toString().isEmpty()) {
            etUserName.setError("Introduce el Nombre");
            itsValid = false;
        }
        if (etUserAddress.getText().toString().isEmpty()) {
            etUserAddress.setError("Introduce la Direccion");
            itsValid = false;
        }

        if (etUserAge.getText().toString().isEmpty()) {
            etUserAge.setError("Introduce tu Edad");
            itsValid = false;
        }

        if (userSex == null) {
            Toast.makeText(context, "Selecciona tu Sexo", Toast.LENGTH_LONG).show();
            itsValid = false;
        }

        return itsValid;
    }

}
